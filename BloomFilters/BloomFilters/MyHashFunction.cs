﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloomFilters
{
    [Serializable]
    public abstract class MyHashFunction
    {
        /// <summary>
        /// Ilość bucketów dostępnych przez funkcję hashującą.
        /// </summary>
        public long M { get; private set; }
        public MyHashFunction(long m)
        {
            M = m;
        }
    }
}
