﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloomFilters
{
    public class PrimeNumberGenerator
    {
        private static bool IsNumberPrime(long numberToTest)
        {
            long maxDivisorToTest = (long)Math.Ceiling(Math.Sqrt(numberToTest));
            for (long i = 2; i <= maxDivisorToTest; i++)
                if (numberToTest % i == 0)
                    return false;
            return true;
        }

        public static long ReturnPrimeNumberBiggerThan(long biggerThan)
        {
            long candidateForPrimeNumber = biggerThan + 1;
            while (!IsNumberPrime(candidateForPrimeNumber))
                candidateForPrimeNumber++;
            return candidateForPrimeNumber;
        }
    }
}
