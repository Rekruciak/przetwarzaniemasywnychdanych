﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloomFilters
{
    /// <summary>
    /// Metoda polegająca na hashowaniu za pomocą jedynie dzielenia modulo przez daną zadaną liczbę zdefiniowaną przy
    /// inicjalizacji obiektu funkcji hashującej.
    /// </summary>
    public class DivisionHash : MyHashFunction, IMyHashFunction
    {
        public DivisionHash(int m) : base(m)
        {
        }

        public int Hash(long input)
        {
            return (int)(input % M);
        }
    }
}
