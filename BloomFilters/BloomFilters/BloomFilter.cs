﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloomFilters
{
    [Serializable]
    public class BloomFilter
    {
        BitArray _bitArray;
        List<IMyHashFunction> _hashFunctions = new List<IMyHashFunction>();

        public BloomFilter(int sizeOfArray, int numberOfHashingFunctions, IEnumerable<long> subfieldNumbers, long maxRange)
        {
            _bitArray = new BitArray(sizeOfArray);
            for (int i = 0; i < numberOfHashingFunctions; i++)
                _hashFunctions.Add(new ParameterizableHashFunction(sizeOfArray, maxRange));
            foreach (var item in subfieldNumbers)
                AddElement(item);
        }

        public void AddElement(long number)
        {
            foreach (var item in _hashFunctions)
                _bitArray[item.Hash(number)] = true;
        }

        public bool Contains(long number)
        {
            return _hashFunctions.All(q => _bitArray[q.Hash(number)]);
        }
    }
}
