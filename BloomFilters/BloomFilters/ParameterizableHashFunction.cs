﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloomFilters
{
    [Serializable]
    public class ParameterizableHashFunction : MyHashFunction, IMyHashFunction
    {
        /// <summary>
        /// Mnożnik wartości inputowanej używany w funkcji g_a_b(x)
        /// </summary>
        public readonly long _a;
        /// <summary>
        /// Wartość modulo funcji g_a_b(x)
        /// </summary>
        public readonly long _b;
        /// <summary>
        /// Liczba pierwsza pobierana używana przy operacji modulo dla funkcji g_a_b(x)
        /// </summary>
        public readonly long _p;
        public ParameterizableHashFunction(long M, long m, long a, long b) : base(m)
        {
            _a = a;
            _b = b;
            _p = PrimeNumberGenerator.ReturnPrimeNumberBiggerThan(M);
        }

        public ParameterizableHashFunction(long m, long originalDataSize)
            : this(originalDataSize,
                  m,
                  RandomNumbersGenerator.GenerateNext(1, originalDataSize - 1 > Int32.MaxValue ? Int32.MaxValue : (int)(originalDataSize - 1)),
                  RandomNumbersGenerator.GenerateNext(0, originalDataSize - 1 > Int32.MaxValue ? Int32.MaxValue : (int)(originalDataSize - 1)))
        { }

        public int Hash(long input)
        {
            return (int)(((_a * (double)input + _b) % _p) % M);
        }
    }
}
