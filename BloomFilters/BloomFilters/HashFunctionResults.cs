﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloomFilters
{
    public class HashFunctionResults
    {
        public int InputValue { get; private set; }
        public int Result { get; private set; }

        public HashFunctionResults(int inputValue, IMyHashFunction hashFunction)
        {
            InputValue = inputValue;
            Result = hashFunction.Hash(inputValue);
        }
    }
}
