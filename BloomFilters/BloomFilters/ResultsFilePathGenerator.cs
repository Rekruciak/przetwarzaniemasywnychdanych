﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloomFilters
{
    public class ResultsFilePathGenerator
    {
        public static string GenerateResultFileFullPath(string fileName)
        {
            if (!fileName.Contains('.'))
                fileName += ".txt";
            return Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), fileName);
        }
    }
}
