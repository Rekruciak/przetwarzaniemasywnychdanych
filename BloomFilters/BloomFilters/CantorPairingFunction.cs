﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloomFilters
{
    public class CantorPairingFunction
    {
        public static long Map(long firstNumber, long secoundNumber)
        {
            return ((firstNumber + secoundNumber) * (firstNumber + secoundNumber + 1)) / 2 + secoundNumber;
        }
    }
}
