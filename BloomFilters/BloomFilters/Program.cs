﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloomFilters
{
    class Program
    {
        private static IEnumerable<int> GetAllNumbers(int M)
        {
            for (int i = 0; i < M; i++)
                yield return i;
        }

        private static IEnumerable<int> GetOddNumbers(int M)
        {
            for (int i = 0; i < M; i++)
                yield return i * 2 + 1;
        }

        private static IEnumerable<int> GetEvenNumbers(int M)
        {
            for (int i = 0; i < M; i++)
                yield return i * 2;
        }

        private int[] _mOptions = new int[]
        {
            10,
            1000,
            100_000,
        };

        private List<(string dataName, int[] data)> _sourceDatas = new List<(string, int[])>
        {
            //("All_numbers", GetAllNumbers(Convert.ToInt32(Math.Pow(10, 8) - 1)).ToArray()),
            //("Odd_numbers", GetOddNumbers(Convert.ToInt32(Math.Pow(10, 8) - 1)).ToArray()),
            //("Even_numbers", GetEvenNumbers(Convert.ToInt32(Math.Pow(10, 8) - 1)).ToArray()),
            //("MSDC_data", MSDC.ReadAllSongsIdFromSourceFile())
        };

        private void HandleDivisionHash((string dataName, IEnumerable<int> data) data, int m)
        {
            DivisionHash divisionHash = new DivisionHash(m);
            List<HashFunctionResults> hashFunctionResults_DivisionHash = new List<HashFunctionResults>();

            Stopwatch stopwatch = Stopwatch.StartNew();
            foreach (var number in data.data)
                hashFunctionResults_DivisionHash.Add(new HashFunctionResults(number, divisionHash));
            stopwatch.Stop();

            Zad_1_ResultsGenerator.GenerateResultsAndPrintThemToFile(
                $"Zad_1_DivisionHash_{data.dataName}_m_{m}",
                stopwatch.ElapsedMilliseconds,
                hashFunctionResults_DivisionHash,
                m);
        }

        private void HandleParametarizableHash((string dataName, IEnumerable<int> data) data, int m)
        {
            ParameterizableHashFunction parameterizableHashFunction = new ParameterizableHashFunction(m, data.data.Count());
            List<HashFunctionResults> hashFunctionResults_ParametarizableHash = new List<HashFunctionResults>();

            Stopwatch stopwatch = Stopwatch.StartNew();
            foreach (var number in data.data)
                hashFunctionResults_ParametarizableHash.Add(new HashFunctionResults(number, parameterizableHashFunction));
            stopwatch.Stop();

            Zad_1_ResultsGenerator.GenerateResultsAndPrintThemToFile(
                $"Zad_1_ParametarizableHash_a_{parameterizableHashFunction._a}_b_{parameterizableHashFunction._b}_p_{parameterizableHashFunction._p}_{data.dataName}_m_{m}",
                stopwatch.ElapsedMilliseconds,
                hashFunctionResults_ParametarizableHash,
                m);
        }

        private void Zadanie_1()
        {
            foreach (var data in _sourceDatas)
                foreach (var m in _mOptions)
                {
                    HandleDivisionHash(data, m);
                    HandleParametarizableHash(data, m);
                }
        }

        private void Zadanie_2()
        {
            int r = 100_000_000; // wielkość całej dziedziny
            int n = 1_000_000; // wielkość badanego podzbioru S
            int m = 8 * n; // długość bitArraya filtra Blooma
            int k = 2; // Ilość funkcji mieszających filtra Blooma

            Zad_2_ResultsGenerator.GenerateResultsAndPrintThemToFile(r, n, m, k);
        }

        private void Zadanie_3()
        {
            var sourceFileRows = MSDC.ReadAllRowsFromSourceFile();

            var allUsersIds = sourceFileRows.Select(q => q.UserId).Distinct().ToArray();
            var allSongsIds = sourceFileRows.Select(q => q.SongId).Distinct().ToArray();

            int allSongsCount = allSongsIds.Count();
            Random rand = new Random();

            int maxUserId = allUsersIds.Max();
            int maxSongId = allSongsIds.Max();

            Stopwatch stopwatch = Stopwatch.StartNew();
            HashSet<(int, int)> hashSet = new HashSet<(int, int)>();
            foreach (var row in sourceFileRows)
                hashSet.Add((row.UserId, row.SongId));
            stopwatch.Stop();
            int creatingHashSetTimeInMilisecounds = (int)stopwatch.ElapsedMilliseconds;

            var allPairsMapped = sourceFileRows.Select(q => CantorPairingFunction.Map(q.UserId, q.SongId)).ToArray();

            stopwatch = Stopwatch.StartNew();
            BloomFilter bloomFilter = new BloomFilter(sourceFileRows.Count() * 10, 8, allPairsMapped, allPairsMapped.Max());
            stopwatch.Stop();
            int creatingBloomFilterTimeInMilisecounds = (int)stopwatch.ElapsedMilliseconds;

            List<(int userId, int songId)> dataToTest2 = new List<(int userId, int songId)>();
            foreach (var item in allUsersIds)
                for (int i = 0; i < 100; i++)
                {
                    int songId = allSongsIds[rand.Next(0, allSongsCount - 1)];
                    dataToTest2.Add((item, songId));
                }

            stopwatch = Stopwatch.StartNew();
            foreach (var item in dataToTest2)
                hashSet.Contains((item.userId, item.songId));
            stopwatch.Stop();
            int hashSetCheckingTimeInMilisecounds = (int)stopwatch.ElapsedMilliseconds;

            stopwatch = Stopwatch.StartNew();
            foreach (var item in dataToTest2)
                bloomFilter.Contains(CantorPairingFunction.Map(item.userId, item.songId));
            stopwatch.Stop();
            int bloomFilterCheckingTimeInMilisecounds = (int)stopwatch.ElapsedMilliseconds;

            int TP = 0;
            int FP = 0;
            int TN = 0;
            int FN = 0;

            foreach (var item in dataToTest2)
            {
                bool bloomFilterContains = bloomFilter.Contains(CantorPairingFunction.Map(item.userId, item.songId));
                bool hashSetContains = hashSet.Contains((item.userId, item.songId));
                if (bloomFilterContains)
                {
                    if (hashSetContains)
                        TP++;
                    else
                        FP++;
                }
                else
                {
                    if (hashSetContains)
                        FN++;
                    else
                        TN++;
                }
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($@"Bloom filter init time in milisecounds: {creatingBloomFilterTimeInMilisecounds}");
            sb.AppendLine($@"HashSet init time in milisecounds: {creatingHashSetTimeInMilisecounds}");
            sb.AppendLine();
            sb.AppendLine($@"Bloom filter checking time in milisecounds: {bloomFilterCheckingTimeInMilisecounds}");
            sb.AppendLine($@"HashSet checking time in milisecounds: {hashSetCheckingTimeInMilisecounds}");
            sb.AppendLine();
            sb.AppendLine($@"Memory storage taken");
            sb.AppendLine($@"Calculated for Bloom: {ObjectSizeCalculationService.CalculateSize(bloomFilter)}");
            sb.AppendLine($@"Calculated for HashSet: {ObjectSizeCalculationService.CalculateSize(hashSet)}");
            sb.AppendLine();
            sb.AppendLine($@"TP: {TP}");
            sb.AppendLine($@"TPR: {TP / (double)(TP + FN)}");
            sb.AppendLine();
            sb.AppendLine($@"TN: {TN}");
            sb.AppendLine($@"TNR: {TN / (double)(TN + FP)}");
            sb.AppendLine();
            sb.AppendLine($@"FN: {FN}");
            sb.AppendLine($@"FNR: {FN / (double)(TP + FN)}");
            sb.AppendLine();
            sb.AppendLine($@"FP: {FP}");
            sb.AppendLine($@"FPR: {FP / (double)(TN + FP)}");

            File.WriteAllText(ResultsFilePathGenerator.GenerateResultFileFullPath("Zad_3_results"), sb.ToString());


        }

        static void Main(string[] args)
        {
            Program program = new Program();
            //program.Zadanie_1();
            program.Zadanie_2();
            //program.Zadanie_3();
        }
    }
}
