﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloomFilters
{
    public class RandomNumbersGenerator
    {
        private static Random _random { get; set; }
        public static int GenerateNext(int min, int max)
        {
            if (_random == null)
                _random = new Random();
            return _random.Next(min, max);
        }
    }
}
