﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloomFilters
{
    public interface IMyHashFunction
    {
        int Hash(long input);
    }
}
