﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloomFilters
{
    public class Zad_1_ResultsGenerator
    {
        private static IEnumerable<int> ReturnKeysMappedToSpecificValue(
            IEnumerable<HashFunctionResults> hashFunctionResults, 
            int specificResult)
        {
            return hashFunctionResults.
                Where(q => q.Result == specificResult).
                Select(q => q.InputValue);
        }

        private static void AppendKeysOf1To9Buckets(
            StringBuilder stringBuilder,
            IEnumerable<HashFunctionResults> hashFunctionResults)
        {
            stringBuilder.AppendLine($@"Bucket wynikowy + wartości źródłowe:");
            for (int i =0; i< 10; i++)
            {
                var keys = ReturnKeysMappedToSpecificValue(hashFunctionResults, i).ToArray();
                stringBuilder.AppendLine($"{i} ({keys.Count()}) {String.Join(", ", keys)}");
            }
        }

        private static void AppendEntropyCalculations(
            StringBuilder stringBuilder,
            int m,
            IEnumerable<(int bucketNumber, double propability)> resultsTransformedToPropabilityOfBuckets)
        {
            stringBuilder.AppendLine($"Wyniki entropii.");
            double optimalEntropy = (-1) * Math.Log(1 / (double)m);
            double entropy = (-1) * resultsTransformedToPropabilityOfBuckets.Sum(q =>
                q.propability * Math.Log(q.propability));
            stringBuilder.AppendLine($"Entropia optymalna: {optimalEntropy}");
            stringBuilder.AppendLine($"Entropia otrzymana: {entropy}");
            stringBuilder.AppendLine($"Różnica: {optimalEntropy-entropy}");
        }

        private static void AppendMeanSquareErrorCalculations(
            StringBuilder stringBuilder,
            int m,
            IEnumerable<(int bucketNumber, double propability)> resultsTransformedToPropabilityOfBuckets)
        {
            stringBuilder.AppendLine($"Błąd średniokwadratowy: {1 / (double)m * resultsTransformedToPropabilityOfBuckets.Sum(q => Math.Pow((q.propability - 1/(double)m), 2))}");
        }

        private static string GenerateResultFileFullPath(string fileName)
        {
            if (!fileName.Contains('.'))
                fileName += ".txt";
            return Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), fileName);
        }

        private static IEnumerable<(int bucketNumber, double propability)> TransformResultsToPropabilityOfBuckets(
            IEnumerable<HashFunctionResults> hashFunctionResults,
            int m)
        {
            double numberOfResults = (double)hashFunctionResults.Count();

            return hashFunctionResults.GroupBy(q => q.Result).Select(q => (q.Key, q.Count() / numberOfResults)).ToArray();
        }

        public static void GenerateResultsAndPrintThemToFile(
            string fileName, 
            double executionTimeInMilisecounds, 
            IEnumerable<HashFunctionResults> hashFunctionResults,
            int m)
        {
            var resultsTransformedToPropabilityOfBuckets = TransformResultsToPropabilityOfBuckets(hashFunctionResults, m);

            StringBuilder resultFileContent = new StringBuilder();
            resultFileContent.AppendLine($"Execution time in milisecounds: {executionTimeInMilisecounds}.");

            resultFileContent.AppendLine();
            AppendKeysOf1To9Buckets(resultFileContent, hashFunctionResults);
            resultFileContent.AppendLine();

            resultFileContent.AppendLine();
            AppendEntropyCalculations(resultFileContent, m, resultsTransformedToPropabilityOfBuckets);
            resultFileContent.AppendLine();


            resultFileContent.AppendLine();
            AppendMeanSquareErrorCalculations(resultFileContent, m, resultsTransformedToPropabilityOfBuckets);
            resultFileContent.AppendLine();


            File.WriteAllText(GenerateResultFileFullPath(fileName), resultFileContent.ToString());
        }
    }
}
