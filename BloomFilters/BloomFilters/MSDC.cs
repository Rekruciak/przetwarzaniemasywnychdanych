﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloomFilters
{
    public class MSDC
    {
        public static string _fullFilePath = Path.GetFullPath(
    Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
        "facts-nns.csv"));

        public static int[] ReadAllSongsIdFromSourceFile()
        {
            return File.ReadAllLines(_fullFilePath).
                Skip(1).
                Select(q => q.Split(',')).
                Select(q => Convert.ToInt32(q[1])).
                Distinct().
                ToArray();
        }

        public static MSDC_Row[] ReadAllRowsFromSourceFile()
        {
            return File.ReadAllLines(_fullFilePath).
                Skip(1).
                Select(q => q.Split(',')).
                Select(q => new MSDC_Row
                {
                    UserId = Convert.ToInt32(q[0]),
                    SongId = Convert.ToInt32(q[1])
                }).
                Distinct().
                ToArray();
        }
    }
}
