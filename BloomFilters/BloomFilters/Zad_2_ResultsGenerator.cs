﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloomFilters
{
    public class Zad_2_ResultsGenerator
    {
        private static string GenerateResultFileFullPath(string fileName)
        {
            if (!fileName.Contains('.'))
                fileName += ".txt";
            return Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), fileName);
        }

        public static void GenerateResultsAndPrintThemToFile(
            int r,
            int n,
            int m,
            int k)
        {
            var numbersOfSubdomain = Zad_2_SubfieldGenerator.GenerateRandom(n, 0, r - 1);

            Stopwatch stopwatch = Stopwatch.StartNew();
            BloomFilter bloomFilter = new BloomFilter(m, k, numbersOfSubdomain.ConvertAll(i => (long)i), r);
            stopwatch.Stop();
            long milisecoundsForBuildingBloomFilter = stopwatch.ElapsedMilliseconds;

            stopwatch = Stopwatch.StartNew();
            HashSet<int> set = new HashSet<int>(numbersOfSubdomain);
            stopwatch.Stop();
            long milisecoundsForBuildingHashSet = stopwatch.ElapsedMilliseconds;

            int TP = 0;
            int FP = 0;
            int TN = 0;
            int FN = 0;

            for (int i=0; i<r; i++)
            {
                bool bloomFilterContains = bloomFilter.Contains(i);
                bool hashSetContains = set.Contains(i);
                if (bloomFilterContains)
                {
                    if (hashSetContains)
                        TP++;
                    else
                        FP++;
                }
                else
                {
                    if (hashSetContains)
                        FN++;
                    else
                        TN++;
                }
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($@"Size of domain: {r}");
            sb.AppendLine($@"Size of subdomain: {n}");
            sb.AppendLine($@"Size of Bloom filters BitArray: {m}");
            sb.AppendLine($@"Number of Bloom filter hashing functions: {k}");
            sb.AppendLine();
            sb.AppendLine($@"Bloom filter init time in milisecounds: {milisecoundsForBuildingBloomFilter}");
            sb.AppendLine($@"HashSet init time in milisecounds: {milisecoundsForBuildingHashSet}");
            sb.AppendLine();
            sb.AppendLine($@"Memory storage taken");
            sb.AppendLine($@"Theoretically for Bloom: {m} bits.");
            sb.AppendLine($@"Theoretically for HashSet: 32*{n} bits.");
            sb.AppendLine($@"Calculated for Bloom: {ObjectSizeCalculationService.CalculateSize(bloomFilter)}");
            sb.AppendLine($@"Calculated for HashSet: {ObjectSizeCalculationService.CalculateSize(set)}");
            sb.AppendLine();
            sb.AppendLine($@"TP: {TP}");
            sb.AppendLine($@"TPR: {TP / (double)(TP + FN)}");
            sb.AppendLine();
            sb.AppendLine($@"TN: {TN}");
            sb.AppendLine($@"TNR: {TN / (double)(TN + FP)}");
            sb.AppendLine();
            sb.AppendLine($@"FN: {FN}");
            sb.AppendLine($@"FNR: {FN / (double)(TP + FN)}");
            sb.AppendLine();
            sb.AppendLine($@"FP: {FP}");
            sb.AppendLine($@"FPR: {FP / (double)(TN + FP)}");

            File.WriteAllText(GenerateResultFileFullPath("Zad_2_results"), sb.ToString());
        }
    }
}
