﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04
{
    public class Person
    {
        public int Id;
        public Person(int id)
        {
            Id = id;
        }

        public List<int> ListenedSongs = new List<int>();

        //public List<Person> PossibleNeighbours { get; set; } = new List<Person>();

        public long[] Signature;

        public List<(double jaccardFactor, Person person)> ClosestNeighbours = new List<(double jaccardFactor, Person person)>();
    }
}
