﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04
{
    public class RNG
    {
        private static Random _random = new Random();
        public static int GenerateNext(int min, int max)
        {
            return _random.Next(min, max);
        }
    }
}
