﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04
{
    public class Zad2
    {
        private static Stopwatch stopwatch;
        public static void Calculate()
        {
            // Dla danego n (np 100) pierwszych użytkowników należy 
            // za pomocą sygnatury minHashowej o długości d (np 100)
            // wyznaczyć przybliżone prawdopodobieństwo Jaccarda.
            //
            // Również obliczyć prawdopodobieństwo prawdziwe i dla każdego wszystkich par z prawdopodobieństwem > 0 
            // obliczyć błąd średniokwadratowy


            // Dla każdego użytkownika obliczamy jego sygnaturę, czyli bierzemy id piosenek i wrzucamy w 
            // funkcje minhashujące

            int d = 100; //signature lenght
            int n = 100; //number of users to calculate

            stopwatch = Stopwatch.StartNew();
            var users = InputFileReader.GenerateListUsers();
            stopwatch.Stop();
            Console.WriteLine($@"Reading list of users: {stopwatch.ElapsedMilliseconds}ms.");

            stopwatch = Stopwatch.StartNew();
            int maxSongId = users.Max(q => q.ListenedSongs.Max(p => p));
            MinhashJaccardCalculator minhashJaccardCalculator = new MinhashJaccardCalculator(maxSongId, d);
            foreach (var user in users)
                minhashJaccardCalculator.CalculateSignature(user);
            stopwatch.Stop();
            Console.WriteLine($@"Generating signatures: {stopwatch.ElapsedMilliseconds}ms.");

            stopwatch = Stopwatch.StartNew();
            for (int i = 0; i < n; i++)
            {
                List<(double jaccardFactor, Person person)> results = new List<(double, Person)>();
                var userToFindClosestNeighbour = users[i];


                foreach (var user in users)
                {
                    if (user.Id == userToFindClosestNeighbour.Id)
                        continue;

                    results.Add((CalculateJaccardFactor.Calculate(userToFindClosestNeighbour, user), user));
                }

                userToFindClosestNeighbour.ClosestNeighbours = 
                    results.OrderByDescending(q => q.jaccardFactor).Take(100).ToList();
            }
            stopwatch.Stop();
            Console.WriteLine($@"Jaccard factor for {n} users and 100 neighbours precise way: {stopwatch.ElapsedMilliseconds}ms.");


            stopwatch = Stopwatch.StartNew();
            double bladSredniokwadratowy = 0d;
            for (int i = 0; i < n; i++)
            {
                var userToFindClosestNeighbour = users[i];
                foreach (var item in userToFindClosestNeighbour.ClosestNeighbours)
                {
                    double aproxJaccard = CalculateJaccardFactor.AproximateFactorByMinhashSignatures(userToFindClosestNeighbour, item.person, d);
                    bladSredniokwadratowy += Math.Pow(aproxJaccard - item.jaccardFactor, 2);
                }
            }
            stopwatch.Stop();
            Console.WriteLine($@"Jaccard factor for {n} users and 100 neighbours aprox way: {stopwatch.ElapsedMilliseconds}ms.
Błąd średniokwadratowy: {bladSredniokwadratowy}");


        }
    }
}
