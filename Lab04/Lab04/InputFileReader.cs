﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04
{
    public class InputFileReader
    {
        public static string _fullFilePath = Path.GetFullPath(
            Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                "facts-nns.csv"));

        public static Person[] GenerateListUsers()
        {
            return File.ReadAllLines(_fullFilePath).
                Skip(1).
                Select(q => q.Split(',')).
                GroupBy(q => q[0],
                q => q[1],
                (key, g) => new Person(Convert.ToInt32(key))
                {
                    ListenedSongs = g.Select(int.Parse).Distinct().ToList()
                }).ToArray();
        }

        public static List<(int songId, List<int> usersListenedToThisSong)> GenerateListOfSongsWithUsersListenedToIt()
        {
            return File.ReadAllLines(_fullFilePath).
                Skip(1).
                Select(q => q.Split(',')).
                GroupBy(q => q[1],
                q => q[0],
                (key, g) =>
                (Convert.ToInt32(key), new List<int>(g.Select(int.Parse).Distinct()))).ToList();
        }

    }
}
