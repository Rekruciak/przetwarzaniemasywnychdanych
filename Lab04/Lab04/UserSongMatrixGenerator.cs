﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04
{
    public class UserSongMatrixGenerator
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="users"></param>
        /// <returns></returns>
        public static bool[,] GenerateMatrixFromFile(IEnumerable<Person> users)
        {
            int numberOfUsers = users.Count();
            int numberOfSongs = users.Max(q => q.ListenedSongs.Max());
            bool[,] matrix = new bool[numberOfUsers, numberOfSongs];
            return matrix;
        }
    }
}
