﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Lab04.BuildHeapOfClosestNeighbours.Heap.HeapNode;

namespace Lab04
{
    public class BuildHeapOfClosestNeighbours
    {
        public class Heap
        {
            public class HeapNode
            {
                public class HeapNodeValue
                {
                    public Person User { get; set; }
                    public double JaccardFactor { get; set; }
                }

                public HeapNodeValue NodeValue { get; set; }
                public HeapNode LeftNode;
                public HeapNode RightNode;
                public HeapNode Parent;
                public Heap HeapObject;

                public HeapNode(Heap heap, HeapNode parent, HeapNodeValue nodeValue)
                {
                    HeapObject = heap;
                    Parent = parent;
                    NodeValue = nodeValue;
                }

                internal void SwitchHeapNodesWithParentIfNecessary()
                {
                    if (Parent == null)
                        return;

                    if (Parent.NodeValue.JaccardFactor < NodeValue.JaccardFactor)
                    {
                        var temp = Parent.NodeValue;
                        Parent.NodeValue = NodeValue;
                        NodeValue = temp;
                        Parent.SwitchHeapNodesWithParentIfNecessary();
                    }
                }

                public void ReplaceNodeWithProperChild()
                {
                    if (LeftNode?.NodeValue == null && RightNode?.NodeValue == null)
                        return;

                    if (LeftNode?.NodeValue == null)
                    {
                        NodeValue = RightNode.NodeValue;
                        RightNode.NodeValue = null;
                        RightNode.ReplaceNodeWithProperChild();
                        return;
                    }
                    else if (RightNode?.NodeValue == null)
                    {
                        NodeValue = LeftNode.NodeValue;
                        LeftNode.NodeValue = null;
                        LeftNode.ReplaceNodeWithProperChild();
                        return;
                    }

                    if (LeftNode.NodeValue.JaccardFactor > RightNode.NodeValue.JaccardFactor)
                    {
                        NodeValue = LeftNode.NodeValue;
                        LeftNode.NodeValue = null;
                        LeftNode.ReplaceNodeWithProperChild();
                    }
                    else
                    {
                        NodeValue = RightNode.NodeValue;
                        RightNode.NodeValue = null;
                        RightNode.ReplaceNodeWithProperChild();
                    }
                }

                public void AddChild(HeapNodeValue nodeValue, int levelsToGoDeeper, int positionCouter)
                {
                    levelsToGoDeeper--;
                    if (levelsToGoDeeper > 0)
                    {
                        if (positionCouter >= Math.Pow(2, levelsToGoDeeper))
                        {
                            positionCouter -= (int)Math.Pow(2, levelsToGoDeeper);
                            RightNode.AddChild(nodeValue, levelsToGoDeeper, positionCouter);
                        }
                        else
                        {
                            LeftNode.AddChild(nodeValue, levelsToGoDeeper, positionCouter);
                        }
                    }
                    else
                    {
                        if (positionCouter == 0)
                        {
                            LeftNode = new HeapNode(HeapObject, this, nodeValue);
                            LeftNode.SwitchHeapNodesWithParentIfNecessary();
                        }
                        else if (positionCouter == 1)
                        {
                            RightNode = new HeapNode(HeapObject, this, nodeValue);
                            RightNode.SwitchHeapNodesWithParentIfNecessary();
                        }
                        else
                            throw new Exception($"Final level and position counter > 1 ({positionCouter})");
                    }
                }
            }

            public Person OwnerOfTheHeap;
            public HeapNode TopNode;
            public int NodeCounters = 1;
            public Heap(Person ownerOfTheHeap, HeapNodeValue heapNodeValue)
            {
                OwnerOfTheHeap = ownerOfTheHeap;
                TopNode = new HeapNode(this, null, heapNodeValue);
            }

            internal void AddNewNode(HeapNodeValue heapNodeValue)
            {
                NodeCounters++;
                int level = (int)Math.Floor(Math.Log(NodeCounters, 2));
                int idFromLeft = NodeCounters - (int)Math.Pow(2, level);
                TopNode.AddChild(heapNodeValue, level, idFromLeft);
            }

            private HeapNodeValue GetHeapTopValueAndRebuildHeap()
            {
                HeapNodeValue ret = TopNode.NodeValue;

                TopNode.ReplaceNodeWithProperChild();

                return ret;
            }

            public List<HeapNodeValue> GetTopHeapValues (int numberOfNodes)
            {
                List<HeapNodeValue> heapNodeValues = new List<HeapNodeValue>();

                for (int i = 0; i < numberOfNodes; i++)
                    heapNodeValues.Add(i < NodeCounters
                        ? GetHeapTopValueAndRebuildHeap()
                        : new HeapNodeValue
                        {
                            User = new Person(-1),
                            JaccardFactor = 0
                        });

                return heapNodeValues;
            }
        }

        public static Heap BuildHeap(Person userToBuildHeapFor, Person[] allUsers)
        {
            Heap heap = null;
            foreach (var user in allUsers)
            {
                if (user.Id == userToBuildHeapFor.Id)
                    continue;

                HeapNodeValue heapNodeValue = new HeapNodeValue
                {
                    User = user,
                    JaccardFactor = CalculateJaccardFactor.Calculate(userToBuildHeapFor, user)
                };

                if (heapNodeValue.JaccardFactor == 0)
                    continue;

                if (heap == null)
                    heap = new Heap(userToBuildHeapFor, heapNodeValue);
                else
                    heap.AddNewNode(heapNodeValue);
            }

            return heap;
        }
    }
}
