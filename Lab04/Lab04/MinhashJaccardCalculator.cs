﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04
{
    public class MinhashJaccardCalculator
    {
        public class MinhashFunctions
        {
            int _a;
            int _b;
            int _maxRange;
            public readonly int Id;

            public MinhashFunctions(int id, int maxRange)
            {
                Id = id;
                _a = RNG.GenerateNext(1, maxRange);
                _b = RNG.GenerateNext(0, maxRange);
                _maxRange = maxRange;
            }

            public long CalculateValueForSpecificRow(long rowId)
            {
                return (_a * rowId + _b) % _maxRange;
            }
        }

        private List<MinhashFunctions> AllMinhashFunctions = new List<MinhashFunctions>();

        int _singatureLenght;
        public MinhashJaccardCalculator(int maxRowId, int signatureLenght)
        {
            _singatureLenght = signatureLenght;
            for (int i = 0; i < signatureLenght; i++)
                AllMinhashFunctions.Add(new MinhashFunctions(i, maxRowId));
        }

        public void CalculateSignature(Person person)
        {
            person.Signature = Enumerable.Repeat(long.MaxValue, _singatureLenght).ToArray();// new long[_singatureLenght] { (long)Int32.MaxValue};
            foreach (var songId in person.ListenedSongs)
            {
                int index = 0;
                foreach (var minhashFunction in AllMinhashFunctions)
                {
                    long newValue = minhashFunction.CalculateValueForSpecificRow(songId);
                    if (newValue < person.Signature[index])
                        person.Signature[index] = newValue;
                    index++;
                }
            }
        }
    }
}
