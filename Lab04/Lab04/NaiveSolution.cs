﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Lab04.BuildHeapOfClosestNeighbours;

namespace Lab04
{
    public class NaiveSolution
    {
        public class FirstTaskAnswer
        {
            public Person UserToFindClosestNeighbour;
            public Person ClosestNeighbour;
            public double JaccardFactor;
        }

        public class Result
        {
            public Person UserToFindClosestNeighbour;
            public List<(double, Person)> Results = new List<(double, Person)>();
        }

        public class JaccardNeighbourComparer : IComparer<double>
        {
            public int Compare(double x, double y)
            {
                return 0 - Comparer<double>.Default.Compare(x, y);
            }
        }

        public static void FindClosest100NeighboursFor100FirstUsers()
        {
            var users = InputFileReader.GenerateListUsers();

            List<Result> finalResults = new List<Result>();

            stopwatch = Stopwatch.StartNew();
            for (int i = 0; i < 100; i++)
            {
                List<(double jaccardFactor, Person person)> results = new List<(double, Person)>();
                var userToFindClosestNeighbour = users[i];

                
                foreach (var user in users)//.Where(w => songs.Where(q => userToFindClosestNeighbour.ListenedSongs.Contains(q.songId)).Any(q => q.usersListenedToThisSong.Contains(w.Id))))
                {
                    if (user.Id == userToFindClosestNeighbour.Id)
                        continue;

                    results.Add((CalculateJaccardFactor.Calculate(userToFindClosestNeighbour, user), user));
                }
                finalResults.Add(new Result
                {
                    UserToFindClosestNeighbour = userToFindClosestNeighbour,
                    Results = results.OrderByDescending(q => q.jaccardFactor).Take(100).ToList(),
                });
            }
            stopwatch.Stop();
            Console.WriteLine($@"FindClosest100NeighboursFor100FirstUsers finished in {stopwatch.ElapsedMilliseconds} milisecounds.");
        }

        private static Stopwatch stopwatch;
        public static void FindClosestNeighboursFor100FirstUsers()
        {
            var users = InputFileReader.GenerateListUsers();

            List<FirstTaskAnswer> results = new List<FirstTaskAnswer>();

            stopwatch = Stopwatch.StartNew();

            for (int i = 0; i < 100; i++)
            {
                double jaccardFactor = 0.0d;
                var userToFindClosestNeighbour = users[i];
                Person closestNeighbour = null;

                foreach (var user in users)
                {
                    if (user.Id == userToFindClosestNeighbour.Id)
                        continue;

                    var tempJaccardFactor = CalculateJaccardFactor.Calculate(userToFindClosestNeighbour, user);
                    if (tempJaccardFactor > jaccardFactor)
                    {
                        jaccardFactor = tempJaccardFactor;
                        closestNeighbour = user;
                    }
                }

                results.Add(new FirstTaskAnswer
                {
                    UserToFindClosestNeighbour = userToFindClosestNeighbour,
                    ClosestNeighbour = closestNeighbour,
                    JaccardFactor = jaccardFactor
                });
            }

            StringBuilder sb = new StringBuilder("User\tNeighbour\tJaccard");
            sb.AppendLine();

            foreach (var result in results)
                sb.AppendLine($"{result.UserToFindClosestNeighbour.Id}\t{result.ClosestNeighbour.Id}\t{result.JaccardFactor}");

            stopwatch.Stop();
            sb.AppendLine($@"Execution time in milisecounds: {stopwatch.ElapsedMilliseconds}.");

            File.WriteAllText("FirstTaskResults.txt", sb.ToString());

        }

        public static void FindClosestNeighboursOfSpecificUser(int userId = 1, int numberOfNeighbours = 100)
        {
            var users = InputFileReader.GenerateListUsers();

            var specificUser = users.FirstOrDefault(q => q.Id == userId);
            if (specificUser == null)
                throw new Exception($"Wybrany user nie istnieje.");

            Stopwatch sw = Stopwatch.StartNew();
            var results = users.
                        OrderBy(q => CalculateJaccardFactor.Calculate(specificUser, q)).
                        Take(numberOfNeighbours).ToArray();
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds + "ms");


            sw = Stopwatch.StartNew();
            Heap heap = BuildHeapOfClosestNeighbours.BuildHeap(specificUser, users);
            var results2 = heap.GetTopHeapValues(numberOfNeighbours);
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds + "ms");


        }
    }
}
