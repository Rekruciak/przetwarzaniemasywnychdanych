﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04
{
    public class CalculateJaccardFactor
    {
        //public static double Calculate(Person firstPerson, Person secoundPerson)
        //{
        //    return firstPerson.ListenedSongs.Intersect(secoundPerson.ListenedSongs).Count()
        //        / Convert.ToDouble(firstPerson.ListenedSongs.Union(secoundPerson.ListenedSongs).Count());
        //}

        //static Random random = new Random();

        public static double Calculate(Person firstPerson, Person secoundPerson)
        {
            //return random.NextDouble();
            return firstPerson.ListenedSongs.Intersect(secoundPerson.ListenedSongs).Count()
                / Convert.ToDouble(firstPerson.ListenedSongs.Union(secoundPerson.ListenedSongs).Count());
        }

        public static double AproximateFactorByMinhashSignatures(Person firstPerson, Person secoundPerson, int signatureLenght)
        {
            int licznik = 0;
            double mianownik = signatureLenght;
            for (int i = 0; i < signatureLenght; i++)
                if (firstPerson.Signature[i] == secoundPerson.Signature[i])
                    licznik++;
            return licznik / mianownik;
        }
    }
}
