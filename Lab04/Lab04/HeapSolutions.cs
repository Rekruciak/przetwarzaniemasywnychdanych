﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Lab04.BuildHeapOfClosestNeighbours;

namespace Lab04
{
    public class HeapSolutions
    {
        private static Stopwatch stopwatch;
        public static void FindClosest100NeighboursForAllUsers()
        {
            StringBuilder sb = new StringBuilder();
            var users = InputFileReader.GenerateListUsers();
            foreach (var user in users)
            {
                Heap heap = BuildHeapOfClosestNeighbours.BuildHeap(user, users);
                var results = heap.GetTopHeapValues(100);
                sb.AppendLine($"User: {user.Id}");
                foreach (var result in results)
                    sb.AppendLine($"{result.User}: {result.JaccardFactor}");
            }
            File.WriteAllText("100NeighboursForAllUsersResults.txt", sb.ToString());
        }

        public static void FindClosest100NeighboursFor100Users()
        {
            StringBuilder sb = new StringBuilder();
            var users = InputFileReader.GenerateListUsers();
            stopwatch = Stopwatch.StartNew();
            foreach (var user in users.Take(100))
            {
                Heap heap = BuildHeapOfClosestNeighbours.BuildHeap(user, users);
                var results = heap.GetTopHeapValues(100);
                sb.AppendLine($"User: {user.Id}");
                foreach (var result in results)
                    sb.AppendLine($"{result.User}: {result.JaccardFactor}");
            }
            stopwatch.Stop();
            sb.AppendLine($@"Execution time in milisecounds: {stopwatch.ElapsedMilliseconds}.");
            File.WriteAllText("100NeighboursFor100UsersResultsHeapSolution.txt", sb.ToString());
        }

        public static void FindClosestNeighboursOfSpecificUser(int userId = 1, int numberOfNeighbours = 100)
        {
            var users = InputFileReader.GenerateListUsers();


            Stopwatch sw = Stopwatch.StartNew();
            var specificUser = users.FirstOrDefault(q => q.Id == userId);
            if (specificUser == null)
                throw new Exception($"Wybrany user nie istnieje.");
            Heap heap = BuildHeapOfClosestNeighbours.BuildHeap(specificUser, users);
            var results = heap.GetTopHeapValues(numberOfNeighbours);
            sw.Stop();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($@"{numberOfNeighbours} najbliższych sąsiadów dla użytkonika: {userId}
w ciągu {sw.ElapsedMilliseconds}ms");
            foreach (var item in results)
                sb.AppendLine($@"{item.User.Id}: {item.JaccardFactor}");
            File.WriteAllText($"{numberOfNeighbours}_ResultsFor_{userId}_User.txt", sb.ToString());
        }
    }
}
