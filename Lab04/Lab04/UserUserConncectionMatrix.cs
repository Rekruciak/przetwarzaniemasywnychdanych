﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab04
{
    public class UserUserConncectionMatrix
    {
        public static bool[,] GenerateEmptyMatrix(int numberOfUsers)
        {
            return new bool[numberOfUsers, numberOfUsers];
        }
    }
}
